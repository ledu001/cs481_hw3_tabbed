﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;


namespace CS481_HW3_TABBED.Views
{
    [DesignTimeVisible(false)]
    public partial class MonsterPage : ContentPage
    {

        public MonsterPage()
        {
            InitializeComponent();


            BindingContext = this;
        }


        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey("monster"))
            {
                var source = Xamarin.Forms.Application.Current.Properties["monster"] as string;
                test.Source = source;
            }
            await test.FadeTo(1, 7000, Easing.Linear);
        }


        protected override void OnDisappearing()
        {
            //We change the monster for next time
            Random generator = new Random();
            base.OnDisappearing();
            Xamarin.Forms.Application.Current.Properties["monster"] = $"monster{generator.Next(1, 5)}.png";
        }
    }
}