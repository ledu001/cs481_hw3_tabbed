﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CS481_HW3_TABBED.Views;

namespace CS481_HW3_TABBED.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {

        public ItemsPage()
        {
            InitializeComponent();
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new MonsterPage()));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        async void OnMonsterClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new MonsterPage());
        }
    }
}